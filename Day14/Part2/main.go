package main

import (
	"bufio"
	"fmt"
	"log"
	"math/big"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	logPrefix = "Advent of Code Day14 Part2: "
	fileName  = "input.txt"
)

var reg *regexp.Regexp

func init() {
	var err error
	reg, err = regexp.Compile("\\d+")
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.Lmicroseconds|log.Lshortfile|log.Llongfile)
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	var mask string
	memoryMap := make(map[string]*big.Int, 0)

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "mask") {
			arr := strings.Split(line, " ")
			mask = arr[len(arr)-1]
			continue
		}

		bufMemoryMap := getMemAddressAndValues(mask, line)
		for k, v := range bufMemoryMap {
			memoryMap[k] = v
		}
	}

	sumBig := new(big.Int)
	for _, v := range memoryMap {
		sumBig = sumBig.Add(sumBig, v)
	}

	log.Println(sumBig)
	log.Println(time.Since(start))
}

func getMemAddressAndValues(mask, line string) map[string]*big.Int {
	memAddress, value := parseMemAddressAndValue(line)
	binMemAddress := fmt.Sprintf("%036b", memAddress)

	srcCombination := applyingMask(mask, binMemAddress)
	return getPossibleCombinations(srcCombination, value)
}

func getPossibleCombinations(srcCombination string, value int) map[string]*big.Int {
	combinationsArr := [][]string{[]string{}}
	for _, char := range srcCombination {
		for index, combination := range combinationsArr {
			if char == 88 {
				newCombination := make([]string, len(combination))
				copy(newCombination, combination)

				combination = append(combination, "0")
				combinationsArr[index] = combination

				newCombination = append(newCombination, "1")
				combinationsArr = append(combinationsArr, newCombination)

				continue
			}
			combination = append(combination, string(char))
			combinationsArr[index] = combination
		}
	}

	bigValue := big.NewInt(int64(value))
	m := make(map[string]*big.Int)
	for _, combination := range combinationsArr {
		s := strings.Join(combination, "")
		m[s] = bigValue
	}
	return m
}

func applyingMask(mask, num string) string {
	result := &strings.Builder{}
	for i, s := range mask {
		switch s {
		case 48: //0
			result.WriteString(string(num[i]))
		case 49: //1
			result.WriteString("1")
		case 88: //X
			result.WriteString("X")
		}
	}
	return result.String()
}

func parseMemAddressAndValue(s string) (int, int) {
	arr := strings.Split(s, "=")
	v := strings.TrimSpace(arr[len(arr)-1])
	value, err := strconv.Atoi(v)
	if err != nil {
		log.Fatalln(err)
	}

	key, err := strconv.Atoi(reg.FindString(arr[0]))
	if err != nil {
		log.Fatalln(err)
	}
	return key, value
}
