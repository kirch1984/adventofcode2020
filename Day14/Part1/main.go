package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	logPrefix = "Advent of Code Day14 Part1: "
	fileName  = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.Lmicroseconds|log.Lshortfile|log.Llongfile)
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	var mask string
	m := make(map[int]int64, 0)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "mask") {
			arr := strings.Split(line, " ")
			mask = arr[len(arr)-1]
			continue
		}
		key, value := getMemAddressAndValues(mask, line)
		m[key] = value
	}

	var sum int64
	for _, v := range m {
		sum += v
	}

	log.Println(sum)
	log.Println(time.Since(start))
}

func getMemAddressAndValues(mask, line string) (int, int64) {
	memAddress, value := parseMemAddressAndValue(line)

	binValue := fmt.Sprintf("%036b", value)
	srcCombination := applyingMask(mask, binValue)

	i, err := strconv.ParseInt(srcCombination, 2, 64)
	if err != nil {
		log.Fatalln(err)
	}
	return memAddress, i
}

func applyingMask(mask, num string) string {
	result := &strings.Builder{}
	for i, s := range mask {
		switch s {
		case 48: //0
			result.WriteString("0")
		case 49: //1
			result.WriteString("1")
		case 88: //X
			result.WriteString(string(num[i]))
		}
	}
	return result.String()
}

func parseMemAddressAndValue(s string) (int, int) {
	arr := strings.Split(s, "=")
	v := strings.TrimSpace(arr[len(arr)-1])
	value, err := strconv.Atoi(v)
	if err != nil {
		log.Fatalln(err)
	}

	r, err := regexp.Compile("\\d+")
	if err != nil {
		log.Fatalln(err)
	}
	key, err := strconv.Atoi(r.FindString(arr[0]))
	if err != nil {
		log.Fatalln(err)
	}
	return key, value
}
