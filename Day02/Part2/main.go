package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	logPrefix = "Advent of Code Day2 Part2: "
	fileInput = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	f, err := os.Open(fileInput)
	if err != nil {
		log.Fatalln()
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	scanner := bufio.NewScanner(f)
	var valid, invalid int

	for scanner.Scan() {
		line := scanner.Text()
		arr := strings.Split(line, " ")
		password := arr[2]
		rangeArr := strings.Split(arr[0], "-")
		min, _ := strconv.Atoi(rangeArr[0])
		max, _ := strconv.Atoi(rangeArr[1])
		char := strings.Split(arr[1], ":")[0]

		switch {
		case char == string(password[min-1]):
			if char != string(password[max-1]) {
				valid++
				continue
			}
		case char == string(password[max-1]):
			if char != string(password[min-1]) {
				valid++
				continue
			}
		}
		invalid++
	}

	log.Println("Valid:", valid)
	log.Println("Invalid:", invalid)
	log.Printf("%v", time.Since(start))
}
