package main

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	logPrefix = "Advent of Code Day4 Part2: "
	fileInput = "input.txt"
)

type validateField func(map[string]string) bool

type validatePassport struct {
	log        *log.Logger
	hclPattern *regexp.Regexp
}

func NewValidatePassport(log *log.Logger) *validatePassport {
	return &validatePassport{
		log:        log,
		hclPattern: regexp.MustCompile("#[a-f0-9]{6}"),
	}
}

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	f, err := os.Open(fileInput)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	vp := NewValidatePassport(log)
	scanner := bufio.NewScanner(f)
	arr := make([]string, 0)
	var valid int

	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			if vp.checkPassport(arr) {
				valid++
			}
			arr = make([]string, 0)
			continue
		}

		arr = append(arr, strings.Split(line, " ")...)
	}
	if vp.checkPassport(arr) {
		valid++
	}

	log.Println(valid)
	log.Println(time.Since(start))
}

func (vp *validatePassport) checkPassport(arr []string) bool {
	passport := convertArrToMap(arr)

	return validatePassportFields(passport,
		vp.checkBYR,
		vp.checkIYR,
		vp.checkEYR,
		vp.checkHGT,
		vp.checkECL,
		vp.checkPID,
		vp.checkHCL,
	)
}

func validatePassportFields(passport map[string]string, funcs ...validateField) bool {
	for _, f := range funcs {
		if !f(passport) {
			return false
		}
	}
	return true
}

func convertArrToMap(arr []string) map[string]string {
	passport := make(map[string]string, len(arr))

	for _, s := range arr {
		kvArr := strings.Split(s, ":")
		passport[kvArr[0]] = kvArr[1]
	}
	return passport
}

func (vp *validatePassport) checkBYR(passport map[string]string) bool {
	if v, ok := passport["byr"]; !ok {
		vp.log.Printf("byr nof found: %v", passport)
		return false
	} else {
		byr, err := strconv.Atoi(v)
		if err != nil {
			return false
		}
		if byr < 1920 || byr > 2003 {
			vp.log.Printf("byr field has wrong value, %v < 1920 || %v > 2003", byr, byr)
			return false
		}
	}

	return true
}

func (vp *validatePassport) checkIYR(passport map[string]string) bool {
	if v, ok := passport["iyr"]; !ok {
		vp.log.Printf("iyr nof found: %v", passport)
		return false
	} else {
		iyr, err := strconv.Atoi(v)
		if err != nil {
			return false
		}
		if iyr < 2010 || iyr > 2020 {
			vp.log.Printf("iyr field has wrong value, %v < 2010 || %v > 2020", iyr, iyr)
			return false
		}
	}

	return true
}

func (vp *validatePassport) checkEYR(passport map[string]string) bool {
	if v, ok := passport["eyr"]; !ok {
		vp.log.Printf("eyr nof found: %v", passport)
		return false
	} else {
		eyr, err := strconv.Atoi(v)
		if err != nil {
			return false
		}
		if eyr < 2020 || eyr > 2030 {
			vp.log.Printf("eyr field has wrong value, %v < 2020 || %v > 2030", eyr, eyr)
			return false
		}
	}

	return true
}

func (vp *validatePassport) checkHGT(passport map[string]string) bool {
	if v, ok := passport["hgt"]; !ok {
		vp.log.Printf("hgt nof found: %v", passport)
		return false
	} else {
		switch {
		case strings.Contains(v, "in"):
			arr := strings.Split(v, "in")
			hgt, err := strconv.Atoi(arr[0])
			if err != nil {
				return false
			}
			if hgt < 59 || hgt > 76 {
				vp.log.Printf("hgt field has wrong in value, %v < 59 || %v > 76", hgt, hgt)
				return false
			}
		case strings.Contains(v, "cm"):
			arr := strings.Split(v, "cm")
			hgt, err := strconv.Atoi(arr[0])
			if err != nil {
				return false
			}
			if hgt < 150 || hgt > 193 {
				vp.log.Printf("hgt field has wrong cm value, %v < 150 || %v > 193", hgt, hgt)
				return false
			}
		default:
			vp.log.Printf("hgt field has wrong value %v, not in, not cm", v)
			return false
		}
	}

	return true
}

func (vp *validatePassport) checkECL(passport map[string]string) bool {
	eyeColor := map[string]struct{}{"amb": {}, "blu": {}, "brn": {}, "gry": {}, "grn": {}, "hzl": {}, "oth": {}}

	if ecl, ok := passport["ecl"]; !ok {
		vp.log.Printf("ecl nof found: %v", passport)
		return false
	} else {
		if _, ok := eyeColor[ecl]; !ok {
			vp.log.Printf("ecl field has wrong value %v", ecl)
			return false
		}
	}

	return true
}

func (vp *validatePassport) checkPID(passport map[string]string) bool {
	if pid, ok := passport["pid"]; !ok {
		vp.log.Printf("pid nof found: %v", passport)
		return false
	} else {
		if len(pid) != 9 {
			vp.log.Printf("pid field has wrong length %v", pid)
			return false
		}
		_, err := strconv.Atoi(pid)
		if err != nil {
			vp.log.Printf("pid field has wrong value %v", pid)
			return false
		}
	}

	return true
}

func (vp *validatePassport) checkHCL(passport map[string]string) bool {
	if v, ok := passport["hcl"]; !ok {
		vp.log.Printf("hcl nof found: %v", passport)
		return false
	} else {
		match := vp.hclPattern.Match([]byte(v))
		if !match {
			vp.log.Printf("hcl field has wrong value: %v", v)
			return false
		}
	}

	return true
}
