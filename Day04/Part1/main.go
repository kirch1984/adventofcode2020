package main

import (
	"bufio"
	"log"
	"os"
	"strings"
	"time"
)

const (
	logPrefix = "Advent of Code Day4 Part1: "
	fileInput = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	f, err := os.Open(fileInput)
	if err != nil {
		log.Fatalln(err)
	}

	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	scanner := bufio.NewScanner(f)
	arr := make([]string, 0)
	var valid int

	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			if checkValid(arr) {
				valid++
			}
			arr = make([]string, 0)
			continue
		}
		arr = append(arr, strings.Split(line, " ")...)
	}
	if checkValid(arr) {
		valid++
	}

	log.Println(valid)
	log.Println(time.Since(start))
}

func checkValid(arr []string) bool {
	model := map[string]struct{}{
		"byr": {},
		"iyr": {},
		"eyr": {},
		"hgt": {},
		"hcl": {},
		"ecl": {},
		"pid": {},
		//"cid":{},
	}

	keyMap := make(map[string]string, len(arr))

	for _, s := range arr {
		kvArr := strings.Split(s, ":")
		keyMap[kvArr[0]] = kvArr[1]
	}

	for k, _ := range model {
		if _, ok := keyMap[k]; !ok {
			return false
		}
	}

	return true
}
