package main

import (
	"bufio"
	"log"
	"os"
	"time"
)

const (
	logPrefix = "Advent of Code Day3 Part1: "
	fileInput = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.LstdFlags|log.Lshortfile|log.Lmicroseconds)

	f, err := os.Open(fileInput)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	var position, sum int
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := scanner.Text()
		if line[position] == 35 { // #=35
			sum++
		}

		position = position + 3

		switch {
		case position == len(line):
			position = 0
		case position > len(line)-1:
			position = position - len(line)
		}
	}

	log.Println("sum: ", sum)
	log.Println(time.Since(start))
}
