package main

import (
	"bufio"
	"log"
	"os"
	"time"
)

const (
	logPrefix = "Advent of Code Day3 Part2: "
	fileInput = "input.txt"
)

type travelRules struct {
	right int
	down  int
}

func newTravelRules(right, down int) *travelRules {
	return &travelRules{right: right, down: down}
}

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.LstdFlags|log.Lshortfile|log.Lmicroseconds)

	f, err := os.Open(fileInput)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	arr := make([]string, 0)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		arr = append(arr, line)

	}

	moveRules := []*travelRules{
		newTravelRules(1, 1),
		newTravelRules(3, 1),
		newTravelRules(5, 1),
		newTravelRules(7, 1),
		newTravelRules(1, 2),
	}

	var s string
	res := 1
	for _, rule := range moveRules {
		var position, sum int
		for i := 0; i < len(arr); i += rule.down {
			s = arr[i]
			if s[position] == 35 { // #=35
				sum++
			}

			position = position + rule.right

			switch {
			case position == len(s):
				position = 0
			case position > len(s)-1:
				position = position - len(s)
			}
		}
		res *= sum
	}

	log.Println(res)
	log.Println(time.Since(start))
}
