package main

import (
	"bufio"
	"log"
	"os"
	"sort"
	"strconv"
	"time"
)

const (
	logPrefix = "Advent of Code Day10 Part2: "
	fileName  = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.Lmicroseconds|log.Lshortfile|log.Llongfile)
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	scanner := bufio.NewScanner(f)
	arr := make([]int, 0)

	for scanner.Scan() {
		line := scanner.Text()
		i, err := strconv.Atoi(line)
		if err != nil {
			log.Fatalf("failed to convert %v to int: %v", line, err)
		}
		arr = append(arr, i)
	}

	//sort and add 0 and last item + 3
	sort.Ints(arr)
	if arr[0] != 0 {
		arr = append([]int{0}, arr...)
	}
	arr = append(arr, arr[len(arr)-1]+3)

	log.Println("pathsSum:", getPathsSum(arr))
	log.Println(time.Since(start))
}

//https://www.reddit.com/r/adventofcode/comments/ka8z8x/2020_day_10_solutions/gfbo61q/?utm_source=reddit&utm_medium=web2x&context=3
func getPathsSum(nums []int) int {
	m := make(map[int]int, 0)
	m[0] = 1

	for _, n := range nums {
		m[n+1] += m[n]
		m[n+2] += m[n]
		m[n+3] += m[n]
	}

	return m[nums[len(nums)-1]+3]
}
