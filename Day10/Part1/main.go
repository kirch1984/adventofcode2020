package main

import (
	"bufio"
	"log"
	"os"
	"sort"
	"strconv"
	"time"
)

const (
	logPrefix = "Advent of Code Day10 Part1: "
	fileName  = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.Lmicroseconds|log.Lshortfile|log.Llongfile)
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	scanner := bufio.NewScanner(f)
	arr := make([]int, 0)
	for scanner.Scan() {
		line := scanner.Text()
		i, err := strconv.Atoi(line)
		if err != nil {
			log.Fatalf("failed to convert %v to int: %v", line, err)
		}
		arr = append(arr, i)
	}

	//sort and add 0 and last item + 3
	sort.Ints(arr)
	if arr[0] != 0 {
		arr = append([]int{0}, arr...)
	}
	arr = append(arr, arr[len(arr)-1]+3)

	log.Println(getResult(arr))
	log.Println(time.Since(start))
}

func getResult(arr []int) int {
	m := make(map[int]int, 0)
	for i := len(arr) - 1; i > 0; i-- {
		if _, ok := m[arr[i]-arr[i-1]]; ok {
			m[arr[i]-arr[i-1]]++
			continue
		}
		m[arr[i]-arr[i-1]] = 1
	}

	return m[3] * m[1]
}
