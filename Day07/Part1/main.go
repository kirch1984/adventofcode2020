package main

import (
	"bufio"
	"log"
	"os"
	"strings"
	"time"
)

const (
	logPrefix = "Advent of Code Day7 Part1: "
	fileName  = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.Lmicroseconds|log.Lshortfile|log.Llongfile)
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	scanner := bufio.NewScanner(f)
	m := make(map[string]struct{}, 0)

	for scanner.Scan() {
		line := scanner.Text()
		m[line] = struct{}{}

	}

	log.Println(findBagColorsByShinyGold(m, "shiny gold"))
	log.Println(time.Since(start))
}

func findBagColorsByShinyGold(rules map[string]struct{}, findBag string) int {
	var result int
	for rule, _ := range rules {
		index := strings.Index(rule, findBag)
		if index > 0 {
			result++
			ruleArr := strings.Split(rule, " ")
			delete(rules, rule)
			result += findBagColorsByShinyGold(rules, strings.Join(ruleArr[:2], " "))
		}
	}

	return result
}
