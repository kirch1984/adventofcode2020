package main

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	logPrefix = "Advent of Code Day7 Part2: "
	fileName  = "input.txt"
)

type regexPatterns struct {
	srcStr   *regexp.Regexp
	oneDig   *regexp.Regexp
	twoWords *regexp.Regexp
}

func newRegexPatterns() *regexPatterns {
	return &regexPatterns{
		srcStr:   regexp.MustCompile("[0-9]\\s\\w+\\s\\w+"),
		oneDig:   regexp.MustCompile("[0-9]"),
		twoWords: regexp.MustCompile("[a-z]+\\s[a-z]+"),
	}
}

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.Lmicroseconds|log.Lshortfile|log.Llongfile)
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	scanner := bufio.NewScanner(f)
	m := make(map[string]struct{}, 0)
	for scanner.Scan() {
		line := scanner.Text()
		m[line] = struct{}{}
	}

	r := newRegexPatterns()
	log.Println(r.findContainBags(m, "shiny gold"))
	log.Println(time.Since(start))
}

func (rp *regexPatterns) findContainBags(rules map[string]struct{}, findBag string) int {
	var result int
	for rule, _ := range rules {
		index := strings.Index(rule, findBag)
		if index == 0 {
			arr := rp.srcStr.FindAllString(rule, len(rule))
			for _, s := range arr {
				countBagStr := rp.oneDig.FindString(s)
				nextFindBag := rp.twoWords.FindString(s)

				countBagInt, err := strconv.Atoi(countBagStr)
				if err != nil {
					log.Fatalf("error to convert string value %v to int: %v", countBagStr, err)
				}
				result += countBagInt + countBagInt*rp.findContainBags(rules, nextFindBag)
			}
		}
	}

	return result
}
