package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"time"
)

const (
	logPrefix = "Advent of Code Day1 Part1: "
	fileInput = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	f, err := os.Open(fileInput)
	if err != nil {
		log.Fatalln()
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	scanner := bufio.NewScanner(f)
	m := make(map[int]int, 0)

	for scanner.Scan() {
		line := scanner.Text()
		num, err := strconv.Atoi(line)
		if err != nil {
			log.Fatalf("failed to convert string %v to int: %v", line, err)
		}
		m[num] = num
	}

	var res int
	for _, n := range m {
		res = 2020 - n
		if _, ok := m[res]; ok {
			delete(m, res)
			delete(m, n)
			log.Println(res * n)
		}
	}

	log.Println(time.Since(start))
}
