package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"time"
)

const (
	logPrefix = "Advent of Code Day1 Part2: "
	fileInput = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	f, err := os.Open(fileInput)
	if err != nil {
		log.Fatalln()
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	numMap := make(map[int]int, 0)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		num, err := strconv.Atoi(line)
		if err != nil {
			log.Fatalf("failed to convert string %v to int: %v", line, err)
		}
		numMap[num] = num

	}

	var res int
	for _, n := range numMap {
		res = 2020 - n
		for _, m := range numMap {
			if v, ok := numMap[res-m]; ok {
				delete(numMap, v)
				delete(numMap, n)
				delete(numMap, m)
				log.Println(v * n * m)
			}
		}
	}

	log.Println(time.Since(start))
}
