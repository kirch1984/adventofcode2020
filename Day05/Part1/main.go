package main

import (
	"bufio"
	"log"
	"os"
	"sort"
	"time"
)

const (
	logPrefix = "Advent of Code Day5 Part1: "
	fileInput = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	f, err := os.Open(fileInput)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	resArr := make([]int, 0)
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := scanner.Text()
		resArr = append(resArr, getSeatID(line))

	}

	sort.Ints(resArr)
	log.Println(resArr[len(resArr)-1])
	log.Println(time.Since(start))
}

func getSeatID(line string) int {
	row := 128
	column := 8
	resRow := 0
	resCol := 0
	for _, c := range line {
		switch string(c) {
		case "F":
			row = row / 2
		case "B":
			row = row / 2
			resRow = resRow + row
		case "R":
			column = column / 2
			resCol = resCol + column
		case "L":
			column = column / 2
		}
	}

	return resRow*8 + resCol
}
