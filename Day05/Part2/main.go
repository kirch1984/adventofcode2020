package main

import (
	"bufio"
	"log"
	"os"
	"sort"
	"time"
)

const (
	logPrefix = "Advent of Code Day5 Part2: "
	fileInput = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	f, err := os.Open(fileInput)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	resArr := make([]int, 0)
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := scanner.Text()
		resArr = append(resArr, getSeatID(line))

	}
	sort.Ints(resArr)

	log.Printf("Max Seat ID: %v", resArr[len(resArr)-1])
	log.Printf("My Seat ID: %v", getMySeatID(resArr))
	log.Println(time.Since(start))
}

func getMySeatID(arr []int) int {
	m := make(map[int]struct{}, len(arr))
	for _, v := range arr {
		m[v] = struct{}{}
	}

	for i := arr[0]; i < arr[len(arr)-1]; i++ {
		if _, ok := m[i]; !ok {
			return i
		}
	}

	return 0
}

func getSeatID(line string) int {
	row := 128
	column := 8
	resRow := 0
	resCol := 0
	for _, c := range line {
		switch c {
		case 70: //F = 70
			row = row / 2
		case 66: //B = 66
			row = row / 2
			resRow = resRow + row
		case 82: //R = 82
			column = column / 2
			resCol = resCol + column
		case 76: //L = 76
			column = column / 2
		}
	}

	return resRow*8 + resCol
}
