package main

import (
	"bufio"
	"log"
	"os"
	"sort"
	"strconv"
	"time"
)

const (
	logPrefix = "Advent of Code Day9 Part2: "
	fileName  = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.Lmicroseconds|log.Lshortfile|log.Llongfile)
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	scanner := bufio.NewScanner(f)
	arr := make([]int, 0)
	for scanner.Scan() {
		line := scanner.Text()
		i, err := strconv.Atoi(line)
		if err != nil {
			log.Fatalf("failed to convert %v to int: %v", line, err)
		}
		arr = append(arr, i)
	}

	preambleLength := 25
	invalidNumber := searchInvalidNumber(preambleLength, arr)
	log.Printf("invalidNumber: %v", invalidNumber)

	log.Printf("sum: %v", findContiguousSet(arr, invalidNumber))
	log.Println(time.Since(start))
}

func findContiguousSet(arr []int, invalidNumber int) int {
	for i, firstItem := range arr {
		res := make([]int, 0)
		res = append(res, firstItem)
		for _, value := range arr[i+1:] {
			firstItem += value
			res = append(res, value)
			if firstItem > invalidNumber {
				break
			}
			if firstItem == invalidNumber {
				sort.Ints(res)
				return res[0] + res[len(res)-1]
			}
		}
	}
	return 0
}

func searchInvalidNumber(preambleLength int, arr []int) int {
	for index, num := range arr[preambleLength:] {
		previousNumbers := getPreviousNumbers(arr[index : preambleLength+index])
		ok := false
		for _, value := range arr[index : preambleLength+index] {
			if _, ok = previousNumbers[num-value]; ok {
				break
			}
		}
		if !ok {
			return num
		}
	}
	return 0
}

func getPreviousNumbers(arr []int) map[int]int {
	m := make(map[int]int, 0)
	for _, value := range arr {
		m[value] = value
	}
	return m
}
