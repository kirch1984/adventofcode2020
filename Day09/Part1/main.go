package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"time"
)

const (
	logPrefix = "Advent of Code Day9 Part1: "
	fileName  = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.Lmicroseconds|log.Lshortfile|log.Llongfile)
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	scanner := bufio.NewScanner(f)
	arr := make([]int, 0)
	for scanner.Scan() {
		line := scanner.Text()
		i, err := strconv.Atoi(line)
		if err != nil {
			log.Fatalf("failed to convert %v to int: %v", line, err)
		}
		arr = append(arr, i)
	}

	preambleLength := 25
	invalidNumber := searchInvalidNumber(preambleLength, arr)

	log.Println(invalidNumber)
	log.Println(time.Since(start))
}

func searchInvalidNumber(preambleLength int, arr []int) int {
	for index, num := range arr[preambleLength:] {
		previousNumbers := getPreviousNumbers(arr[index : preambleLength+index])
		ok := false
		for _, value := range arr[index : preambleLength+index] {
			if _, ok = previousNumbers[num-value]; ok {
				break
			}
		}
		if !ok {
			return num
		}
	}
	return 0
}

func getPreviousNumbers(arr []int) map[int]int {
	m := make(map[int]int, 0)
	for _, value := range arr {
		m[value] = value
	}
	return m
}
