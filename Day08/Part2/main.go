package main

import (
	"bufio"
	"errors"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	logPrefix = "Advent of Code Day8 Part2: "
	fileName  = "input.txt"
)

var errLoop = errors.New("loop detected")

type accumulator struct {
	value        int
	indexStorage map[int]struct{}
}

func newAccumulator() *accumulator {
	return &accumulator{
		value:        0,
		indexStorage: make(map[int]struct{}, 0),
	}
}

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.Lmicroseconds|log.Lshortfile|log.Llongfile)
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	scanner := bufio.NewScanner(f)
	arr := make([]string, 0)
	for scanner.Scan() {
		line := scanner.Text()
		arr = append(arr, line)

	}

	for i := 0; i < len(arr); i++ {
		argArr := strings.Split(arr[i], " ")
		command := argArr[0]
		switch command {
		case "nop":
			countAccumulator(newArr(arr, argArr, i, "jmp"))
		case "jmp":
			countAccumulator(newArr(arr, argArr, i, "nop"))
		}
	}

	log.Println(time.Since(start))
}

func newArr(oldArr, argArr []string, i int, newCommand string) []string {
	b := make([]string, len(oldArr))
	copy(b, oldArr)
	argArr[0] = newCommand
	b[i] = strings.Join(argArr, " ")
	return b
}

func countAccumulator(arr []string) {
	a := newAccumulator()
	index := 0
	for i := 0; i < len(arr)-1; i++ {
		if err := a.detectLoop(index); err == errLoop {
			break
		}

		command, num := a.parseLine(arr[index])
		index += a.nextIndex(command, num)

		if index == len(arr) {
			log.Printf("num of iter %v : index %v : value %v", i+1, index, a.value)
			break
		}
	}
}

func (a *accumulator) nextIndex(command string, num int) int {
	switch command {
	case "nop":
		return 1
	case "jmp":
		return num
	case "acc":
		a.value += num
		return 1
	}
	return 0
}

func (*accumulator) parseLine(line string) (string, int) {
	argArr := strings.Split(line, " ")
	command := argArr[0]
	num, err := strconv.Atoi(argArr[1])
	if err != nil {
		log.Fatalf("failed to parse %v: %v", argArr[1], err)
	}
	return command, num
}

func (a *accumulator) detectLoop(index int) error {
	if _, ok := a.indexStorage[index]; ok {
		return errLoop
	}
	a.indexStorage[index] = struct{}{}
	return nil
}
