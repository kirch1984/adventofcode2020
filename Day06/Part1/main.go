package main

import (
	"bufio"
	"log"
	"os"
	"time"
)

const (
	logPrefix = "Advent of Code Day6 Part1: "
	fileName  = "input.txt"
)

func main() {
	start := time.Now()
	log := log.New(os.Stdout, logPrefix, log.Lmicroseconds|log.Lshortfile|log.Llongfile)
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	scanner := bufio.NewScanner(f)
	var sum int
	arr := make([]string, 0)

	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			sum += countYes(arr)
			arr = make([]string, 0)
			continue
		}
		arr = append(arr, line)
	}
	sum += countYes(arr)

	log.Println(sum)
	log.Println(time.Since(start))
}

func countYes(arr []string) int {
	m := make(map[string]struct{})
	var sum int
	switch {
	case len(arr) == 1:
		sum = len(arr[0])
	default:
		for _, item := range arr {
			for _, ch := range item {
				m[string(ch)] = struct{}{}
			}
		}
		sum = len(m)
	}

	return sum
}
